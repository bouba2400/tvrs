apt-get update -y
apt-get install -y git

[[ ! -z "$CUSTOM_HOSTNAME" ]] && hostnamectl set-hostname $CUSTOM_HOSTNAME || echo "CUSTOM_HOSTNAME not provided"

echo provisioned
