
apt install -y golang
curl https://cli-assets.heroku.com/install-ubuntu.sh | sh

go get -u go.mozilla.org/sops/cmd/sops
cd ~/go/src/go.mozilla.org/sops
make install
ln -sf ~/go/bin/sops /usr/local/bin/sops
cd -

if ! grep "$(cat cicd/etc/setup/ssh/known.hosts 2>/dev/null)" /etc/ssh/ssh_known_hosts > /dev/null; then
    cat cicd/etc/setup/ssh/known.hosts >> /etc/ssh/ssh_known_hosts
fi
if ! grep "$(cat cicd/etc/setup/ssh/custom.conf 2>/dev/null)" /etc/ssh/ssh_config > /dev/null; then
    cat cicd/etc/setup/ssh/custom.conf >> /etc/ssh/ssh_config
fi

echo $SOPS_PGP_SECRET_KEY | gpg --import --armor --allow-secret-key-import

mkdir -p tmp/server-app-heroku-repo
