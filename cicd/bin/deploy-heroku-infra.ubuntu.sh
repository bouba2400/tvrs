
./server/infra/bin/control.heroku.ubuntu.sh init
sops -d "cicd/etc/server/heroku-infra/$1.sops.env" > cicd/etc/server/heroku-infra/.env
./server/infra/bin/control.heroku.ubuntu.sh deploy cicd/etc/server/heroku-infra/.env
rm cicd/etc/server/heroku-infra/.env
