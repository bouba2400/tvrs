
sops -d "cicd/etc/server/heroku-infra/$1.sops.env" > cicd/etc/server/heroku-infra/.env

s=`sh cicd/bin/gen-secret-key.ubuntu.sh`
sed -i 's#TVRS_APP_PARAM_NEXT_PROXY_USER_ACCESS_SECRET_KEY=".*"#TVRS_APP_PARAM_NEXT_PROXY_USER_ACCESS_SECRET_KEY="'"$s"'"#' cicd/etc/server/heroku-infra/.env

sops -e cicd/etc/server/heroku-infra/.env > "cicd/etc/server/heroku-infra/$1.sops.env"
rm cicd/etc/server/heroku-infra/.env

if [ "-$2" = "-y" ]
then
  git add "cicd/etc/server/heroku-infra/$1.sops.env"
  git commit -m "Renew secret keys for $1 deployment"
  git push
fi
