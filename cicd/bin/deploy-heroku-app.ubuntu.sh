
userhd=`eval echo ~$USER`

rm -f "$userhd/.ssh/tvrscicd.rsa" "$userhd/.ssh/tvrscicd.rsa.pub" || echo $?
echo -e 'y\n' | ssh-keygen -f "$userhd/.ssh/tvrscicd.rsa" -t rsa -N ''
cat "$userhd/.ssh/tvrscicd.rsa.pub"
heroku keys:remove "$USER@$(hostname)" || echo $?
heroku keys:add "$userhd/.ssh/tvrscicd.rsa.pub"

rm -Rf tmp/server-app-heroku-repo/*
rm -Rf tmp/server-app-heroku-repo/.*
cd tmp/server-app-heroku-repo
git init
heroku git:remote --ssh-git -a "pp10-$1-$2"
git config user.email "cicd@tvrs.net.test"
git config user.name "CICD TOOL"

git checkout -b master
git pull heroku master || echo $?

if [ "-$1" = "-apigateway" ]
then
  cp ../../server/app/frontend/apigateway/* .
  cp -r ../../server/app/frontend/apigateway/src .

  if [ -z "$CI" ]
  then
    echo "{" > ci.json
    echo "branch: \"$CI_BRANCH\"," >> ci.json
    echo "commit: \"$CI_COMMIT\"," >> ci.json
    echo "buildno: \"$CI_BUILDNO\"" >> ci.json
    echo "}" >> ci.json
    cat ci.json
  fi
else
  cp ../../server/app/backend/* .
  cp -r ../../server/app/backend/gradle .
  mkdir -p workerstarter
  cp ../../server/app/backend/workerstarter/* workerstarter/
  cp -r ../../server/app/backend/workerstarter/src workerstarter/
  mkdir -p realtimeworker
  cp ../../server/app/backend/realtimeworker/* realtimeworker/
  cp -r ../../server/app/backend/realtimeworker/src realtimeworker/
  mkdir -p summaryworker
  cp ../../server/app/backend/summaryworker/* summaryworker/
  cp -r ../../server/app/backend/summaryworker/src summaryworker/
  # mkdir -p searchworker
  # cp ../../server/app/searchworker/* searchworker/
  # cp -r ../../server/app/searchworker/src searchworker/
  # mkdir -p miningworker
  # cp ../../server/app/miningworker/* miningworker/
  # cp -r ../../server/app/miningworker/src miningworker/

  if [ -z "$CI" ]
  then
    echo "branch=$CI_BRANCH" > ci.properties
    echo "commit=$CI_COMMIT" >> ci.properties
    echo "buildno=$CI_BUILDNO" >> ci.properties
    cat ci.properties
  fi
fi

git add .
git commit -m "See commit $3"
git push heroku master
