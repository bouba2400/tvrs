
sops -d "cicd/etc/server/heroku-infra/$1.sops.env" > cicd/etc/server/heroku-infra/.env
set -a; . cicd/etc/server/heroku-infra/.env; set +a

sed -i 's#TVRS_APP_PARAM_CURRENT_PROXY_USER_ACCESS_SECRET_KEY=".*"#TVRS_APP_PARAM_CURRENT_PROXY_USER_ACCESS_SECRET_KEY="'"$TVRS_APP_PARAM_NEXT_PROXY_USER_ACCESS_SECRET_KEY"'"#' cicd/etc/server/heroku-infra/.env
sed -i 's#TVRS_APP_PARAM_NEXT_PROXY_USER_ACCESS_SECRET_KEY=".*"#TVRS_APP_PARAM_NEXT_PROXY_USER_ACCESS_SECRET_KEY=""#' cicd/etc/server/heroku-infra/.env

sops -e cicd/etc/server/heroku-infra/.env > "cicd/etc/server/heroku-infra/$1.sops.env"
rm cicd/etc/server/heroku-infra/.env

if [ "-$2" = "-y" ]
then
  git add "cicd/etc/server/heroku-infra/$1.sops.env"
  git commit -m "Rotate secret keys for $1 deployment"
  git push
fi
