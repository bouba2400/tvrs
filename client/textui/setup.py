#!/usr/bin/env python
import os
from datetime import datetime
from setuptools import setup, find_packages


def buildinfo(buildnum):
    commit = os.environ['CI_COMMIT'] if 'CI_COMMIT' in os.environ else 'NA'
    time = datetime.now().isoformat()
    info = """
    commit = "{commit}"
    buildno = "{buildno}"
    time = "{time}"
    """.format(commit=commit, time=time, buildno=buildnum)

    with open('src/resources/main/.meta/build.ini', 'w') as fh:
        fh.write(info)


with open("versions.txt", "r") as fp:
    lines = fp.readlines()
    version = lines[-1].strip()

buildno = (
    os.environ["CI_BUILDNO"]
    if "CI_BUILDNO" in os.environ
    else datetime.now().strftime("%Y%m%d%H%M%S")
)
xqualifier = "snapshot"

if "CI_BRANCH" in os.environ \
        and (os.environ["CI_BRANCH"] == "master"
             or os.environ["CI_BRANCH"].startswith("release/")
             or os.environ["CI_BRANCH"].startswith("hotfix/")):
    xqualifier = buildno

buildinfo(buildno)
setup(
    install_requires=["click==7.1.2"],
    name="tvrsclient",
    version="{version}+{xqualifier}".format(version=version, xqualifier=xqualifier),
    description="TVRS Text UI",
    author="BD",
    author_email="bd@tvrs.net.test",
    url="https://bitbucket.org/bouba2400/tvrs",
    packages=find_packages("src/python/main"),
    package_dir={"": "src/python/main"},
    entry_points={"console_scripts": ["tvrs=tvrs.client.textui.app:cli"]},
    include_package_data=True,
    data_files=[(".meta", ["src/resources/main/.meta/build.ini"])],
)
