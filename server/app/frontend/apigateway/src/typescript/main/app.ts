import './init';
import Context from './appcontext';
// tslint:disable-next-line: no-var-requires
const gateway = require('fast-gateway');

class App {
  readonly context: Context;
  private server: any;

  constructor() {
    this.context = new Context();
  }

  configure(): App {
    this.context.initialize();
    return this;
  }

  run() {
    this.server = gateway({
      routes: this.context.get('router').getRoutes(),
    });

    this.server.start(process.env.PORT || this.context.get('configprovider').get('server.port'));
  }
}

const app = new App();
app.configure().run();

export default app;
