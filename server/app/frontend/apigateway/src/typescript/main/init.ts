import path = require('path');

const initNodeConfig = () => {
  let resDir:string;
  const environment = process.env.NODE_ENV || 'development';

  if (environment === 'production') {
    resDir =`${__dirname}'/../../resources/`;
  } else {
    resDir = `${__dirname}'/../../resources/main/`;
  }

  const extraDirsRaw = process.env.TVRS_CONFIG_PROFILES || 'dev local';
  const extraDirs = extraDirsRaw.split(' ').map((item) => `${resDir}config/${item}/`);
  const initDirs = [`${resDir}config/`];

  process.env.NODE_CONFIG_DIR = [...initDirs, ...extraDirs].join(path.delimiter);
};

initNodeConfig();
