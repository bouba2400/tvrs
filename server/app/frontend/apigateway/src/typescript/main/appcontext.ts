import config from 'config';
import Router from './router';

export default class AppContext {
  private singletons: { [x: string]: any };

  constructor() {
    this.singletons = {};
  }
  
  public get router() : Router {
    return this.singletons.router
  }

  public get config() : any {
    return this.singletons.config
  } 

  get(name: string) {
    return this.singletons[name];
  }

  initialize() {
    this.singletons.config = config;
    this.singletons.router = new Router(this.singletons.config);
  }
}
