
const PROXY_AUTHORIZATION_HTTP_HEADER = 'Proxy-Authorization';
const BASIC_HTTP_AUTHENTICATION_SCHEME = 'Basic';

export default class Router {
  constructor(readonly config:any) {
  }

  getRoutes() {
    const rtwurl = this.config.get('backend.targets.realtimeworker.url');
    const sywurl = this.config.get('backend.targets.summaryworker.url');
    const accessid = this.config.get('backend.proxyAuth.user.accessId');
    const accesssecret = this.config.get('backend.proxyAuth.user.accessSecretKey');
    const onRequest = async (req:any, res:any) => {
      const auth = `${BASIC_HTTP_AUTHENTICATION_SCHEME} ${accessid}:${accesssecret}`
      req.headers[PROXY_AUTHORIZATION_HTTP_HEADER] = new Buffer(auth).toString('base64')
    };
    const hooks = {
      onRequest
    }
    const route = {
      hooks
    }

    return [
      {
        ...route,
        prefix: '/summary',
        target: `${sywurl}/public`,        
      },
      {
        ...route,
        prefix: '/',
        target: `${rtwurl}/public`,
        pathRegex: '*',
      }
    ];
  }
}
