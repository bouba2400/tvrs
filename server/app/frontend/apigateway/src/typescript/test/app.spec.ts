import 'jest';
jest.mock('fast-gateway')

// tslint:disable-next-line: no-var-requires
const gateway = require('fast-gateway');

const start = jest.fn();
const server = {
  start
};

gateway.mockReturnValue(server);

import app from '~/app'

test('app runs', () => {
  expect(app.context).not.toBe(null);
  expect(app.context.config).not.toBe(null);
  expect(start.mock.calls.length).toBe(1);
})
