const fs = require('fs');
const shell = require('shelljs');

var ciprops = {
  branch: '',
  buildno: new Date().toLocaleDateString().replace(/\//g, ''),
  commit: 'NA'
}

if (fs.existsSync('ci.json')) {
  ciprops = JSON.parse(fs.readFileSync('ci.json'));
}
else if (process.env.CI) {
  ciprops.branch = process.env.CI_BRANCH
  ciprops.commit = process.env.CI_COMMIT
  ciprops.buildno = process.env.CI_BUILDNO
}

fs.readFile('versions.txt', "utf8", (err, data) => {
  if (err) throw err;

  lines = data.split("\n");
  console.log(lines.length);

  if (lines.length > 1) {
    version = lines.slice(-2)[0];
    console.log(version);

    time = new Date().toISOString();
    
    if (ciprops.branch !== '' && (ciprops.branch === 'master' 
          || ciprops.branch.startsWith('release/') 
          || ciprops.branch.startsWith('hotfix/')
        )) {
      version = `${version}+${ciprops.buildno}`;
    }
    else {
      version = `${version}-snapshot`
    }

    rs = shell.exec(`npm version --allow-same-version ${version}`);
    if (rs.code !== 0) {
      shell.exit(1);
    }
    else {
      buildmeta = {
        version,
        commit: ciprops.commit,
        buildno: ciprops.buildno,
        time
      }

      fs.writeFile('src/resources/main/.meta/build.json', JSON.stringify(buildmeta), 'utf8', (err) => {
        if (err) throw err;
      });
    }
  }
});
