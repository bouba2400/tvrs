package net.tvrs.server.realtimeworker;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import lombok.AllArgsConstructor;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;

@SpringBootTest
@AllArgsConstructor
public class RealtimeWorkerApplicationTests {
  @Autowired
  private ApplicationContext context;

  @Test
  public void contextLoads() {
    assertNotNull(context, "context is null");
  }

}
