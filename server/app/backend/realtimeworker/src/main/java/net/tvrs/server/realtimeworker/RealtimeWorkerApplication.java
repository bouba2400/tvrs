package net.tvrs.server.realtimeworker;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.MarkerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SuppressWarnings("PMD.UseUtilityClass")
@SpringBootApplication(scanBasePackages = "net.tvrs.server")
@Slf4j
public class RealtimeWorkerApplication {

  public static void main(final String[] args) {
    try {
      SpringApplication.run(RealtimeWorkerApplication.class, args);
    } catch (Exception e) { //NOPMD
      log.error(MarkerFactory.getMarker("FATAL"), "Application startup error", e);
    }
  }

}
