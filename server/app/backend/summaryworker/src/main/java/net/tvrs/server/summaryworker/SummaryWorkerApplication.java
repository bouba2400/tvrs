package net.tvrs.server.summaryworker;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.MarkerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SuppressWarnings("PMD.UseUtilityClass")
@Slf4j
@SpringBootApplication(scanBasePackages = "net.tvrs.server")
public class SummaryWorkerApplication {

  public static void main(final String[] args) {
    try {
      SpringApplication.run(SummaryWorkerApplication.class, args);
    } catch (Exception e) { //NOPMD
      log.error(MarkerFactory.getMarker("FATAL"), "Application startup error", e);
    }
  }

}
