package net.tvrs.server.web.security;

import java.util.Set;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties("tvrs.web.security")
public class StandardWebSecurityProperties {
  @NotEmpty
  private Set<ProxyUserAccessCredential> proxyUserAccessCredentials;

  @Data
  public static class ProxyUserAccessCredential {
    @NotBlank
    private String id;
    @NotBlank
    private String secretKey;
  }
}
