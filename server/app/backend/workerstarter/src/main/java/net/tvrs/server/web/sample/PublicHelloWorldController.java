package net.tvrs.server.web.sample;

import lombok.NoArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController("/public/hello")
@NoArgsConstructor
public class PublicHelloWorldController {
    @RequestMapping("/world")
    public String world() {
        return "Public Hello World!";
    }
}
