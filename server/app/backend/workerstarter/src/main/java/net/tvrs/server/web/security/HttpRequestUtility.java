package net.tvrs.server.web.security;

import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.util.Base64;
import javax.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

@Slf4j
public class HttpRequestUtility {
  public static final String PROXY_AUTHORIZATION_HEADER = "Proxy-Authorization";
  public static final String BASIC_AUTHENTICATION_SCHEME = "Basic";

  private HttpRequestUtility() {}

  public static String[] extractBasicProxyAuthCredentials(HttpServletRequest request) throws ParseException {
    return extractBasicAuthCredentials(request, PROXY_AUTHORIZATION_HEADER);
  }

  public static boolean isBasicProxyAuthPresent(HttpServletRequest request) {
    return isBasicAuthPresent(request, PROXY_AUTHORIZATION_HEADER);
  }

  public static boolean isBasicAuthPresent(HttpServletRequest request, String headerName) {
    if (!isHeaderPresent(request, headerName)) {
      return false;
    }

    String header = request.getHeader(headerName).trim();
    return StringUtils.startsWithIgnoreCase(header, BASIC_AUTHENTICATION_SCHEME);
  }

  public static boolean isProxyAuthHeaderPresent(HttpServletRequest request) {
    return isHeaderPresent(request, PROXY_AUTHORIZATION_HEADER);
  }

  public static boolean isHeaderPresent(HttpServletRequest request, String headerName) {
    String header = request.getHeader(headerName);
    return header != null;
  }

  public static String[] extractBasicAuthCredentials(HttpServletRequest request, String headerName) throws ParseException {
    if (!isBasicAuthPresent(request, headerName)) {
      throw new IllegalArgumentException(
          String.format("Missing http header %s or missing basic authentication in header", headerName));
    }

    String header = request.getHeader(headerName).trim();
    if (header.equalsIgnoreCase(BASIC_AUTHENTICATION_SCHEME)) {
      throw new ParseException("Empty basic authentication", -1);
    }

    byte[] base64Token = header.substring(6).getBytes(StandardCharsets.UTF_8);
    byte[] decoded;

    try {
      decoded = Base64.getDecoder().decode(base64Token);
    } catch (IllegalArgumentException e) {
      log.error("Error decoding b64 text", e);
      throw new ParseException("Invalid b64 text in basic authentication", -1);
    }

    String token = new String(decoded, StandardCharsets.UTF_8);

    int delim = token.indexOf(":");
    if (delim == -1) {
      throw new ParseException("Missing delimeter in basic authentication", -1);
    }

    return new String[] { token.substring(0, delim), token.substring(delim + 1) };
  }
}
