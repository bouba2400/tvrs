package net.tvrs.server.web.security;

import java.util.Collection;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

public class ProxyAuthenticationToken extends UsernamePasswordAuthenticationToken {
  public ProxyAuthenticationToken(Object principal, Object credentials) {
    super(principal, credentials);
  }

  public ProxyAuthenticationToken(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities) {
    super(principal, credentials, authorities);
  }
}
