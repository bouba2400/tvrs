package net.tvrs.server.web.security;

import java.util.Collections;
import java.util.Optional;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class ProxyAuthenticationProvider implements AuthenticationProvider {
  private final StandardWebSecurityProperties webSecurityProperties;

  @Override
  public Authentication authenticate(Authentication authentication) throws AuthenticationException {
    Optional<StandardWebSecurityProperties.ProxyUserAccessCredential> credential =
        webSecurityProperties.getProxyUserAccessCredentials().stream()
            .filter(el -> el.getId().equals(authentication.getName())
                && el.getSecretKey().equals(authentication.getCredentials())).findFirst();

    if (!credential.isPresent()) {
      throw new BadCredentialsException("Authentication failed");
    }

    return new ProxyAuthenticationToken(authentication.getName(), authentication.getCredentials(), Collections.emptyList());
  }

  @Override
  public boolean supports(Class<?> authentication) {
    return authentication.equals(ProxyAuthenticationToken.class);
  }
}
