package net.tvrs.server.web.security;

import java.io.IOException;
import java.text.ParseException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

@Slf4j
public class ProxyAuthenticationFilter extends OncePerRequestFilter {

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
      throws ServletException, IOException {
    try {
      if (isAuthenticationRequired()) {
        ProxyAuthenticationToken token = extractCreds(request);
        SecurityContextHolder.getContext().setAuthentication(token);
      }

      filterChain.doFilter(request, response);
    } catch (RuntimeException | ParseException e) {
      log.error("Error checking proxy authentication", e);
      response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
    }
  }

  private ProxyAuthenticationToken extractCreds(HttpServletRequest request) throws ParseException {
    String[] creds = HttpRequestUtility.extractBasicProxyAuthCredentials(request);
    return new ProxyAuthenticationToken(creds[0], creds[1]);
  }

  private boolean isAuthenticationRequired() {
    Authentication existingAuth = SecurityContextHolder.getContext().getAuthentication();
    return (existingAuth == null) || !existingAuth.isAuthenticated();
  }
}
