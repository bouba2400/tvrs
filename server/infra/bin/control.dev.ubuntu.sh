
init()
{
	wget -qO- https://get.docker.com/ | sh
	apt-get -y install python-pip
	pip install docker-compose
}

case "$1" in
	init)
		init
		;;
			
	*)
		echo $"Usage: $0 {init}"
		exit 1

esac