
deploy()
{
	envsubst < server/infra/etc/terraform/modules/tvrsheroku/all.envsubst.auto.tfvars > tmp/server-infra-tf-wd/tvrsheroku/all.auto.tfvars
	envsubst < server/infra/etc/terraform/modules/tvrsheroku/main.envsubst.tf > tmp/server-infra-tf-wd/tvrsheroku/main.tf
	cp server/infra/etc/terraform/modules/tvrsheroku/variables.tf tmp/server-infra-tf-wd/tvrsheroku/
	cp server/infra/etc/terraform/modules/tvrsheroku/providers.tf tmp/server-infra-tf-wd/tvrsheroku/
	cp server/infra/etc/terraform/modules/tvrsheroku/resources.tf tmp/server-infra-tf-wd/tvrsheroku/
	cp -r server/infra/etc/terraform/modules/backendapp tmp/server-infra-tf-wd/tvrsheroku/

	terraform init tmp/server-infra-tf-wd/tvrsheroku
	terraform apply -auto-approve tmp/server-infra-tf-wd/tvrsheroku
}

destroy()
{
	echo tbi
}

init()
{
	apt-get update -y
	apt-get install -y unzip gettext-base
	mkdir -p tmp/server-infra-tf
	cd tmp/server-infra-tf
	rm terraform_0.12.24_linux_amd64.zip*
	wget https://releases.hashicorp.com/terraform/0.12.24/terraform_0.12.24_linux_amd64.zip
	unzip terraform_0.12.24_linux_amd64.zip
	mv terraform /usr/local/bin/
	terraform --version

	cd ../..
	mkdir -p tmp/server-infra-tf-wd/tvrsheroku
	envsubst < server/infra/etc/terraform/config.envsubst.hcl > ~/.terraformrc
	ls -al ~/.terraformrc
}

# export TF_CLI_CONFIG_FILE=tmp/server-infra-tf-wd/config.hcl

case "$1" in
	deploy)
		set -a; . "$2"; set +a
		deploy
		;;

	destroy)
		set -o allexport; . "$2"; set +o allexport
		destroy
		;;

	init)
		init
		;;

	*)
		echo $"Usage: $0 {deploy|destroy|init}"
		exit 1

esac
