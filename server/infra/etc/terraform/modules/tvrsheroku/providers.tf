provider "heroku" {
  version = "~> 2.0"
  email   = "${var.heroku_email}"
  api_key = "${var.heroku_api_key}"
}
