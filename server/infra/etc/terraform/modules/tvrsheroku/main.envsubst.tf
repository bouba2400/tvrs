terraform {
  backend "remote" {
    organization = "bouba2400"
    workspaces {
      name = "${TVRS_TF_HEROKU_WORKSPACE}"
    }
  }

  required_providers {
    heroku = ">= 2.0"
  }
}
