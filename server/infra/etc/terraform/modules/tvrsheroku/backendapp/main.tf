locals {
  app_name = "${var.naming_prefix}-${var.name}-${var.naming_suffix}"
}

resource "heroku_config" "main" {
  vars = {
    WEB_CMD = "java -jar ${var.name}/build/libs/*.jar"
    SPRING_PROFILES_ACTIVE   = "${var.app_param_config_profiles}"
    TVRS_WEB_SECURITY_PROXY_USER_ACCESS_CREDENTIALS_0_ID   = "${var.app_param_proxy_user_acccess_id}"
  }

  sensitive_vars = {
    TVRS_WEB_SECURITY_PROXY_USER_ACCESS_CREDENTIALS_0_SECRET_KEY   = "${var.app_param_current_proxy_user_access_secret_key}"
  }
}

resource "heroku_config" "additional_proxy_user" {
  count = var.app_param_next_proxy_user_access_secret_key != "" ? 1 : 0

  vars = {
    TVRS_WEB_SECURITY_PROXY_USER_ACCESS_CREDENTIALS_1_ID   = "${var.app_param_proxy_user_acccess_id}"
  }

  sensitive_vars = {
    TVRS_WEB_SECURITY_PROXY_USER_ACCESS_CREDENTIALS_1_SECRET_KEY   = "${var.app_param_next_proxy_user_access_secret_key}"
  }
}

resource "heroku_app" "oneandonly" {
  name   = "${local.app_name}"
  region = "us"
  config_vars = "${heroku_config.main.vars}"
  sensitive_vars = "${heroku_config.main.sensitive_vars}"
}

resource "heroku_app_config_association" "oneandonly_additional_proxy_user" {
  count = var.app_param_next_proxy_user_access_secret_key != "" ? 1 : 0

  app_id = "${heroku_app.oneandonly.id}"
  vars = "${heroku_config.additional_proxy_user.vars}"
  sensitive_vars = "${heroku_config.additional_proxy_user.sensitive_vars}"
}

output "app_url" {
  value       = "${heroku_app.oneandonly.web_url}"
//  value       = "https://${local.app_name}.herokuapp.com"
  description = "url of app"
}
