variable "naming_prefix" {
  description = "Prefix of app and resource names"
  type        = "string"
}

variable "naming_suffix" {
  description = "Suffix of app and resource names"
  type        = "string"
}

variable "naming_core" {
  description = "Core of app and resource names"
  type        = "string"
}

variable "app_param_config_profiles" {
  description = "Application parameter for active config profile (e.g. 'prod' or 'test debug')"
  type        = "string"
}

variable "app_param_proxy_user_acccess_id" {
  description = "Application parameter for "
  type        = "string"
}

variable "app_param_current_proxy_user_access_secret_key" {
  description = "Application parameter for "
  type        = "string"
}

variable "app_param_next_proxy_user_access_secret_key" {
  description = "Application parameter for "
  type        = "string"
}
