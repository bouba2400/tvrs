
module "realtimeworker" {
  source = "./backendapp"
  naming_core = "realtimeworker"
  naming_prefix = "${var.naming_prefix}"
  naming_suffix = "${var.naming_suffix}"
  app_param_config_profiles = "${var.app_param_config_profiles}"
  app_param_proxy_user_acccess_id = "${var.app_param_proxy_user_acccess_id}"
  app_param_current_proxy_user_access_secret_key = "${var.app_param_current_proxy_user_access_secret_key}"
  app_param_next_proxy_user_access_secret_key = "${var.app_param_next_proxy_user_access_secret_key}"
}

module "summaryworker" {
  source = "./backendapp"
  naming_core = "summaryworker"
  naming_prefix = "${var.naming_prefix}"
  naming_suffix = "${var.naming_suffix}"
  app_param_config_profiles = "${var.app_param_config_profiles}"
  app_param_proxy_user_acccess_id = "${var.app_param_proxy_user_acccess_id}"
  app_param_current_proxy_user_access_secret_key = "${var.app_param_current_proxy_user_access_secret_key}"
  app_param_next_proxy_user_access_secret_key = "${var.app_param_next_proxy_user_access_secret_key}"
}

resource "heroku_app" "apigateway" {
  name   = "${var.naming_prefix}-apigateway-${var.naming_suffix}"
  region = "us"
  config_vars = {
    TVRS_CONFIG_PROFILES   = "${var.app_param_config_profiles}"
    TVRS_REALTIMEWORKER_URL   = "${module.realtimeworker.app_url}"
    TVRS_SUMMARYWORKER_URL   = "${module.summaryworker.app_url}"
    TVRS_PROXY_USER_ACCESS_ID = "${var.app_param_proxy_user_acccess_id}"
    TVRS_PROXY_USER_ACCESS_SECRET_KEY = "${var.app_param_current_proxy_user_access_secret_key}"
  }
}
